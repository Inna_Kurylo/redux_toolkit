export const selectorAppModal = state => state.app.modal;
export const selectorAppFavorites = state => state.app.favorites;
export const selectorAppCart = state => state.app.cart;
export const selectorCurrentProd = state => state.app.currentProd;

export const selectorCards = state => state.cards.cards;