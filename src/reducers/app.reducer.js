import { createSlice } from "@reduxjs/toolkit";

const initialState = {
    favorites: JSON.parse(localStorage.getItem('favorite') || '[]'),
    cart: JSON.parse(localStorage.getItem('cart') || '[]'),
    currentProd: {},
    modal: false,
};

const appSlice = createSlice({
    name:"app",
    initialState,
    reducers: {
        actionAddFavorites: (state, {payload}) => {
            state.favorites = [...state.favorites,payload];
            localStorage.setItem('favorite', JSON.stringify(state.favorites));
        },
        actionAddCart: (state, {payload}) => {
            state.cart = [...state.cart,payload];
            localStorage.setItem('cart', JSON.stringify(state.cart));
        },
        actionCurrentProd: (state, {payload}) => {
            state.currentProd = {...payload}
        },
        actionModal: (state, {payload}) =>{
            state.modal = payload;
        },
        actionRemoveFavorite: (state, {payload}) => { 
            state.favorites = state.favorites.filter(row => row.articul !== payload.articul);
            localStorage.setItem('favorite', JSON.stringify(state.favorites));
        },
        actionRemoveCart: (state, {payload}) => { 
            debugger;
            state.cart = state.cart.filter(row => row.articul !== payload.articul)
            localStorage.setItem('cart', JSON.stringify(state.cart));
        }


    }
})

export const { actionAddCart, actionRemoveCart, actionAddFavorites, actionCurrentProd, actionModal, actionRemoveFavorite } = appSlice.actions;

export default appSlice.reducer;