import appReducer, {actionAddCart, actionRemoveCart, actionCurrentProd, actionModal, actionAddFavorites, actionRemoveFavorite} from "./app.reducer";
import cardsReducer, {actionFetchCards} from "./cards.reducer";

export {
    actionAddCart,
    actionRemoveCart,
    appReducer,
    actionCurrentProd,
    actionModal,
    actionAddFavorites,
    actionRemoveFavorite,
    cardsReducer,
    actionFetchCards,
}