import PropTypes from 'prop-types';
import {ReactComponent as Closing} from './icons/close.svg';

function ButtonClose({click, classWhite}){
    return(
        <button type="button" className="close-delete" onClick={click}>
            <Closing className={classWhite}/>			
        </button>
    )
}

ButtonClose.propTypes = {
	closeModal: PropTypes.func,
};

export default ButtonClose;