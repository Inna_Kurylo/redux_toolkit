import PropTypes from 'prop-types';
import Sidebar from '../Sidebar'
import './Header.scss';

function Header({count, countFav}){
	return (
		<header className="header">
				<Sidebar count={count} countFav={countFav}/>
			
		</header>
	)

}

Header.propTypes = {
	count: PropTypes.number,
	countFav: PropTypes.number,
};

export default Header;
