import {configureStore} from "@reduxjs/toolkit";
import logger from "redux-logger";
import thunk from "redux-thunk";

import { appReducer, cardsReducer } from "../reducers";

const store = configureStore({
    reducer: {
        app: appReducer,
        cards: cardsReducer,
    },
    middleware: (getDefaultMiddleware) => getDefaultMiddleware().concat(logger,thunk),
});

export default store;